EDGE Documentation
==================

Documentation for the EDGE bioinformatics project, a DTRA-funded NGS analysis effort.

* [Read The Docs](http://edge.readthedocs.org/)

* [PDF](https://readthedocs.org/projects/edge/downloads/pdf/latest/)

* [ebook](https://readthedocs.org/projects/edge/downloads/epub/latest/)

![ebook](https://bytebucket.org/nmrcjoe/edge-docs/raw/6615b223c422b26a15826d611b73e8bf7117253e/ebook.png)
